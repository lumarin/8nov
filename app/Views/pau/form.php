<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?= $title?></title>
    </head>
    <body>
        <div class="container">
        <h1 class="text-primary"><?= $title?></h1>
            <form action="<?= site_url('pau')?>" method="post">
                <div class=form-group">
                    <label for="nif">NIF </label>
                    <input type="text" name="NIF" value="" id="NIF" class="form-control"/>
                </div>
                <div class=form-group">
                    <label for="apellido1">1er Apellido </label>
                    <input type="text" name="apellido1" value="" id="apellido1" class="form-control"/>
                </div>
                <div class=form-group">
                    <label for="apellido2">2º Apellido </label>
                    <input type="text" name="apellido2" value="" id="apellido2" class="form-control"/>
                </div>
                <div class=form-group">
                    <label for="nombre">Nombre </label>
                    <input type="text" name="Nombre" value="" id="Nombre" class="form-control"/>
                </div>
                <div class=form-group">
                    <label for="email">Correo electrónico </label>
                    <input type="text" name="email" value="" id="email" class="form-control"/>
                </div>
                <div class=form-group">
                    <label for="ciclo">Ciclo </label>
                    <select name="ciclo" value="" id="ciclo" class="form-control">
                        <option value="441104">CFGS Administración y Finanzas</option>
                        <option value="449104">CFGS Comercio Internacional</option>
                        <option value="472103">CFGM Gestión Administrativa</option>
                        <option value="481104">CFGS Higiene Bucodental</option>
                        <option value="483104">CFGS Prótesis Dentales</option>
                        <option value="707103">CFGM Sistemas Microinformáticos y Redes</option>
                        <option value="710103">CFGM Farmacia y Parafarmacia</option>
                        <option value="829104">CFGS Administración de Sistemas Informáticos en Red</option>
                        <option value="845104">CFGS Desarrollo de Aplicaciones Web</option>
                        <option value="899104">CFGS Gestión de Ventas y Espacios Comerciales</option>
                        <option value="906104">CFGS Asstencia a la Dirección</option>
                        <option value="925103">CFGM Actividades Comerciales</option>
                        <option value="950104">CFGS Ortoprótesis y Productos de Apoyo</option>
                        <option value="975104">CFGS Documentación y Administración Sanitarias</option>
                        <option value="976104">CFGS Imagen para el Diagnóstico y Medicina Nuclear</option>
                        <option value="977104">CFGS Laboratorio Clínico y Biomédico</option>
                        <option value="978104">CFGS Radioterapia y Dosimetría</option>
                    </select>
                    
                </div>
                <div class=form-group">
                    <label for="tasa">Tipo de tasa </label>
                    <input type="text" name="tipo_tasa" value="" id="tipo_tasa" class="form-control"/>
                </div>
                <input type="submit" name="enviar" value="Enviar" />
            
            </form>
        </div>
    </body>
</html>
