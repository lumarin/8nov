<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;


class SolicitudesModel extends Model {
    /* definimos los parámetros de la tabla a la que
     * queremos acceder
     */
    protected $table='pau';
    protected $primaryKey = 'id';
    protected $returnType = 'object'; 
    protected $allowedFields = ['NIF','nombre','apellido1','apellido2','ciclo','tipo_tasa'];
    
}
